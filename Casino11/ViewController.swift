//
//  ViewController.swift
//  Casino11
//
//  Created by Marcin Kępa on 05/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageViewOne: UIImageView!
    
    @IBOutlet weak var imageViewTwo: UIImageView!
    

    @IBAction func rollButtonPressed(_ sender: UIButton) {
        let imageOneNumber = arc4random_uniform(6)
        let imageTwoNumber = arc4random_uniform(6)
        
        let Array = [ #imageLiteral(resourceName: "One"), #imageLiteral(resourceName: "Two"),#imageLiteral(resourceName: "Three"),#imageLiteral(resourceName: "Four"),#imageLiteral(resourceName: "Five"),#imageLiteral(resourceName: "Six")]
        imageViewOne.image =  Array [Int(imageOneNumber)]
        imageViewTwo.image = Array [Int(imageTwoNumber)]
    }
    
    
}

